#!/usr/bin/python3

import sys
import json
from datetime import datetime
import fasteners

def add_wish(address, goal, title):
    global wishlist
    wishlist[0].append({ 
        "title": title,
        "goal": goal,
        "total": 0,
        "contributors": 0,
        "address": address,
        "percent": 0
        }) 
    # description & url are optional and can be added / edited in the json directly

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print(f'Usage: {sys.argv[0]} <Site directory> <XMR Address> <XMR Goal> <Wish Title>')
        sys.exit(4)

    json_file = f'{sys.argv[1]}/data/wishlist.json'
    with fasteners.InterProcessLock(f'/tmp/{json_file.replace("/", "-")}.lock'):
        with open(json_file) as f:
            wishlist = json.load(f)

        add_wish(sys.argv[2], sys.argv[3], sys.argv[4])
        #example:
        #add_wish("8BQE9MKuMt23pfhG4zoH4x8prqb8w1nKVXoDE7yAGXTGFmtawVPA1aaAFvjj7XFjASZtMXuyfhY9uVBP8xxtBVRnRcLgTXe",  0.5, "Dressed as a miner / mining")

        # this should be the same as in the main script:
        wishlist[0] = sorted(wishlist[0], key=lambda k: k['percent'], reverse=True)
        wishlist[1]["modified"] = str(datetime.utcnow())

        with open(json_file, "w") as f:
            json.dump(wishlist, f, indent = 6)

    print("Don't foget to run make_qrs.py after you are done adding in order to generate the QR codes")
