#!/usr/bin/python3

from datetime import datetime
import time
import sys
import json
from monerorpc.authproxy import AuthServiceProxy, JSONRPCException
import fasteners

#json_file = "wishlist.json"
json_file = "/var/www/html/data/wishlist.json"
rpc_url = "http://127.0.0.1:6666/json_rpc"

def main(tx_id):
    transfers = getTx(tx_id, 0)
    for transfer in transfers:
        update(transfer)

def update(transfer):
    if transfer:
        with fasteners.InterProcessLock(f'/tmp/{json_file.replace("/", "-")}.lock'):
            with open(json_file) as f:
                wishlist = json.load(f)
            tx_amount = transfer["amount"]/1000000000000
            for i in range(len(wishlist[0])):
                try:
                    if wishlist[0][i]["address"] == transfer["address"]:
                        found = 1
                        wishlist[0][i]["contributors"] += 1
                        wishlist[0][i]["total"] += tx_amount
                        # can axceed 100%
                        wishlist[0][i]["percent"] = wishlist[0][i]["total"] / wishlist[0][i]["goal"] * 100
                        break
                except Exception as e:
                    raise e
            wishlist[0] = sorted(wishlist[0], key=lambda k: score(k),reverse=True)
            wishlist[1]["modified"] = str(datetime.utcnow())

            with open(json_file, "w") as f:
                json.dump(wishlist, f, indent = 6)

def score(k):
    newF  = 5000 * (1 if ("new" in k) else 0)
    releasedF = 5000 * (-1 if ("progress" in k and k["progress"]=="RELEASED") else 0)
    contributorsF = k['contributors']
    return newF + releasedF + contributorsF

def getTx(tx_id, conf=0):
    retries = 12
    #loop incase rpc daemon has not started up yet. but only 12 times max
    while True:
        try:
            rpc_connection = AuthServiceProxy(rpc_url)
            info = rpc_connection.get_transfer_by_txid({"account_index":0, "txid":str(tx_id)})
            break
        except Exception as e:
            print(e)
            retries -= 1
            if retries <= 0:
                raise e
            print("Retry in in 5 seconds.")
            time.sleep(5)
    # quick zero conf
    if conf == 0:
        if info["transfer"]["height"] == 0:
            return info["transfers"]
    else:
        if info["transfer"]["height"] != 0:
            return info["transfers"]

if __name__ == '__main__':
    main(sys.argv[1])
